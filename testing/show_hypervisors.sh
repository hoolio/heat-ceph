#!/bin/bash

openstack stack resource list -n 5 $1 --filter type=OS::Nova::Server -c physical_resource_id -f value | xargs -n1 openstack server show -c OS-EXT-SRV-ATTR:host -f value
