# How to make a Ceph cluster on OpenStack

# TLDR;
1. `virtualenv openstack`  
1. `. openstack/bin/activate`  
1. `pip install python-openstackclient python-heatclient ansible os_client_config`
1. `. your-tennancy-openrc.sh`
1. `git clone https://gitlab.com/hoolio/heat-ceph.git && cd heat-ceph`
1. `openstack stack create -t global_template.yaml -e environment.yaml ceph`
1. `ansible-playbook ansible-bootstrap.yaml`
1. `ansible-playbook ansible-cephadm.yaml`
1. via the login node, connect to the first mon to run `ceph -s`

## If benchmarking..
Cheap and cheeerful manual benchmarking.  
https://docs.google.com/spreadsheets/d/1fBOIjh_RHp2ifJkiZJUroiiETxmNyN-oRNxYaAnb5yo/edit#gid=0

Execute steps with: 
1. set key, instance_az and volume_az in environment.yaml as appropriate
1. `time openstack stack create -t global_template.yaml -e environment.yaml ceph --wait`
1. `time ansible-playbook ansible-bootstrap.yaml`
1. `time ansible-playbook ansible-cephadm.yaml`
1. `openstack stack output show ceph monitor-nodes-public-ips`
1. ssh to root on the first node and then
1. `ceph osd pool create testbench 100 100`
1. `rados bench -p testbench 60 write --no-cleanup` and record the Bandwidth as (MB/sec)
1. exit that node
1. `time openstack stack delete -y --wait ceph`

# Full instructions
## What this is and isn't
Is: A virtual ceph cluster is a great way to learn ceph.  
Not: anything else really.

## Disclaimer and about heat
Openstack Heat has the ability to do magical (and dangerous) things to your openstack
environment.  in particular it can call and will call many of the openstack APIs
and like anything from the internet, you should really understand what this does
before running it.  

`$ openstack stack delete ceph`  
`Are you sure you want to delete this stack(s) [y/N]? y`

## Architecture
This environment has the following topology:
1. A single login node, that doesn't actually do anything.
1. `mon_count: 3`, the "first" one of which is where most of the ansible orchestration happens.
1. `storage_node_count: 7`, how many storage/OSD nodes.
1. Then for each storage node, `osds_per_storage_node: 4` and `osd_volume_size_in_gb: 5`

The number of mons and storage nodes and the number of OSDs per node and the
size of each OSD volume etc is all configurable and defined in the `environment.yaml`
file.  As is the default image used for nodes and which ssh keys to use and so
on.  Security groups are hardcoded in the template.  Network uses public ip.

## Installation prerequisites
1. an openstack environment :) 
1. a functioning current openstack client, ideally installed in a virtualenv:  
.. `virtualenv openstack`  
.. `. openstack/bin/activate`  
.. `pip install python-openstackclient python-heatclient ansible`  
1. an openstack tennancy with sufficient instance and volume quota.  ideally empty.
1. an open-rc.sh file for the tennancy above
1. a functioning ssh-agent.  if you don't know what that is, you'll need to.
1. understanding of openstack images, default usernames etc

## testing
1. activate your openstack virtual environment
1. source your `openrc.sh` file (openstack credentials)
1. `openstack stack list` should work but show no existing stacks
1. `openstack server list` should work too

leave this session open as we'll be using it in a moment

## create cluster via heat template
1. git clone this repo
.. `git clone https://gitlab.com/hoolio/heat-ceph.git`
1. `cd heat-ceph`
1. have a look at `environment.yaml` and sanitise for your environment.  change ssh key name!
1. `openstack stack create -t global_template.yaml -e environment.yaml ceph`
```
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| id                  | bed657bf-b172-47e9-af0e-2602fef80ceb |
| stack_name          | ceph                                 |
| description         | No description                       |
| creation_time       | 2023-02-22T01:46:08                  |
| updated_time        | None                                 |
| stack_status        | CREATE_IN_PROGRESS                   |
| stack_status_reason |                                      |
+---------------------+--------------------------------------+`
```
1. `openstack stack show ceph`

everything from here assumes the stack built correctly.  Once built you shouldn't
need to revisit the stack again, it should just work.

## Troubleshooting stack builds
Working out why stuff didn't spawn is as fun as it usually is with Openstack.
Heat issues many requests and stresses the environment somewhat.  If there are
any issues building single instances you're not going to have fun with Heat ;)
You should try/use any/all of the following until you get CREATE_COMPLETE
1. Check the `stack_status` and `stack_status_reason` fields.
1. `openstack server list` to see what the status of the build is.
1. Have a look at the instances and volumes under the dashboard.  I find it helps.
1. Try a dry run? `openstack stack create -t global_template.yaml -e environment.yaml ceph --dry-run --fit-width`
1. Delete the stack and try again
..`openstack stack delete ceph`
..`openstack stack create -t global_template.yaml -e environment.yaml ceph`
1. Talk to your usual support channels.  You may need to modify the heat template(s)
for your environment.

## So what did we get??
```
$ openstack server list
+----------------------------------+----------------+--------+-------------------+----------------------------------+
| ID                               | Name           | Status | Networks          | Image Name                       |
+----------------------------------+----------------+--------+-------------------+----------------------------------+
| 32302178-90d5-4217-b882-bca477a9 | storage-1-euk  | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| 2de1                             |                |        |                   | amd64                            |
| 367ea7e6-6e94-4919-9b9f-         | mds-1-euk      | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| aabb66418f2b                     |                |        |                   | amd64                            |
| 0073c756-cba4-4479-a737-db9e1354 | mon-1-euk      | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| a9d0                             |                |        |                   | amd64                            |
| 619eaa80-5415-4ae8-b81a-         | storage-0-euk  | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| c209b76577ae                     |                |        |                   | amd64                            |
| 722c3afa-                        | login-node-euk | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| ea69-4a5e-a084-5669cca6c3a2      |                |        |                   | amd64                            |
| 1434f1f1-74a5-4dcf-a11a-         | mds-0-euk      | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| 98d57f405676                     |                |        |                   | amd64                            |
| 08cf0493-8294-42c3-ae3b-         | mon-0-euk      | ACTIVE | nci=x.x.x.x       | NeCTAR Ubuntu 16.04 LTS (Xenial) |
| 76d89bbfcc11                     |                |        |                   | amd64                            |
+----------------------------------+----------------+--------+-------------------+----------------------------------+
```

### Returning special output data
For convenience, important data about your cluster can be returned thusly:

Note the reference to the first mon, this is the one you will need to ssh to later on.
```
x openstack stack output show ceph mon-nodes-public-ips
+--------------+----------------------------------------------------------------+
| Field        | Value                                                          |
+--------------+----------------------------------------------------------------+
| description  | Mon addresses; `ceph` commands only available on the first one |
| output_key   | mon-nodes-public-ips                                           |
| output_value | ['131.217.173.53', '131.217.173.59', '131.217.173.214']        |
+--------------+----------------------------------------------------------------+
```
But for whatever reason the "first" mon selected by ansible is not always the first mon as represented in this list.  
So i just connect to each and run ceph -s and if it's not installed, then thats not the right mon.

You might not need this, but here is in case.
```
openstack stack output show ceph client-node-public-ip
+--------------+----------------------------------------------------+
| Field        | Value                                              |
+--------------+----------------------------------------------------+
| description  | cephfs client node.                                |
| output_key   | client-node-public-ip                              |
| output_value | ['131.217.173.249']                                |
+--------------+----------------------------------------------------+
```

### Node naming and name resolution
All nodes are named in the form <type>-<serial_number>-<random_suffix_for_this_cluster>.

All nodes have hosts files populated with every node member, this is done by ansible, eg:

```
# cat /etc/hosts
131.217.174.38 mds-1-dmo
131.217.174.171 mds-0-dmo
131.217.172.118 storage-6-dmo
131.217.174.223 mon-1-dmo
131.217.175.79 storage-1-dmo
131.217.172.106 storage-5-dmo
131.217.175.7 storage-3-dmo
131.217.173.3 mon-2-dmo
131.217.172.241 storage-4-dmo
131.217.175.244 mon-0-dmo
131.217.174.74 storage-0-dmo
131.217.174.159 storage-2-dmo
131.217.173.49 login-node-dm
```

### Ansible dynamic inventory
Too smart for me, talk to Andy Bottling.  But once your stack is up you can
get the list of hosts in the stack (like magic!) by running `python ./inventory.py`.

And you will note that in `ansible.cfg` there is a line that specifies inventory
for this ansible repository should be gathered by that means.

If you ran that python you would note that there are neat groups of hosts.  If you look
at `environment.yaml` you will see that we use openstack metadata to store
the detail which will be read back out as ansible groups.  Fancy i know.

```
metadata:
    ansible_host_groups: mons
```

# Setup Ceph cluster

## Bootstrap the cluster with ansible
Before running this, read it so that you know what it's doing.  In short it just
sets up root key ssh auth, and creates the common /etc/hosts file.  we can see here what it does:

```
> ansible-playbook ansible-bootstrap.yaml --list-tasks

playbook: ansible-bootstrap.yaml

  play #1 (all): bootstrap nodes for ceph-deploy        TAGS: []
    tasks:
      make /root/.ssh   TAGS: [root_ssh_auth]
      chmod ceph /root/.ssh/authorized_keys     TAGS: [root_ssh_auth]
      copy over authorized_keys for root        TAGS: [root_ssh_auth]
      Allow root ssh with cert  TAGS: [root_ssh_auth]
      Restart sshd service      TAGS: [root_ssh_auth]
      Remove old /etc/hosts     TAGS: [hosts_file]
      touch /etc/hosts  TAGS: [hosts_file]
      Add the inventory into /etc/hosts TAGS: [hosts_file]
```
exec it with `ansible-playbook ansible-bootstrap.yaml` or you can target a 
subset of tasks using the tag with the `--tags` argument.

If that worked, then the cluster is now ready for ceph!  

## Run cephadm and make things happen!!

```
> ansible-playbook ansible-cephadm.yaml --list-tasks

playbook: ansible-cephadm.yaml

  play #1 (3bad4284-b8e6-4e9f-abcb-46be7e55e29d): from the first mon setup cephadm and bootstrap nodes  TAGS: []
    tasks:
      download current cephadm tool     TAGS: [setup]
      Change permission cephadm TAGS: [setup]
      cephadm add-repo --release reef   TAGS: [repo]
      /usr/sbin/cephadm install TAGS: [install]
      /usr/sbin/cephadm install ceph-common     TAGS: [install]
      /usr/sbin/cephadm bootstrap --mon-ip "{{ ansible_ssh_host }}"     TAGS: [bootstrap]
      fetch the ceph ssh key for distribution to nodes  TAGS: [keys]

  play #2 (all): from the first mon setup cephadm and bootstrap nodes   TAGS: []
    tasks:
      copy ceph ssh key to all nodes    TAGS: [keys]

  play #3 (3bad4284-b8e6-4e9f-abcb-46be7e55e29d): from the first mon setup cephadm and bootstrap nodes  TAGS: []
    tasks:
      add mon hosts     TAGS: [mons]
      label mon hosts   TAGS: [mons]
      apply config to mons      TAGS: [mons]
      add osd hosts     TAGS: [storage]
      add storage hosts TAGS: [storage]
      ceph orch apply osd --all-available-devices       TAGS: [storage]
```

If you're happy with all that then execute it with `ansible-playbook ansible-cephadm.yaml`.  Again, use the tags if you like.

## Connect to your cluster
Cephadm commands will have been executed on the "first" mon, that's the one that's listed first in the inventory. 
But which one is that?  good question.  In order to find out easily just run:

```
x openstack stack output show ceph monitor-nodes-public-ips
+--------------+----------------------------------------------------------------+
| Field        | Value                                                          |
+--------------+----------------------------------------------------------------+
| description  | Mon addresses; `ceph` commands only available on the first one |
| output_key   | mon-nodes-public-ips                                           |
| output_value | ['131.217.173.53', '131.217.173.59', '131.217.173.214']        |
+--------------+----------------------------------------------------------------+
```

Anytime you want to do manual ceph, commands do it from the first node, eg '131.217.173.53'

### Did it work?
From the first mon:
```
# ceph -s
  cluster:
    id:     1a074f16-7cfd-11ee-9c56-3752139ad11f
    health: HEALTH_OK
 
  services:
    mon: 2 daemons, quorum mon-0-dmo,mon-2-dmo (age 65m)
    mgr: mon-1-dmo.ujhcrd(active, since 4h), standbys: mon-2-dmo.qivlxk
    osd: 28 osds: 28 up (since 45m), 28 in (since 47m)
 
  data:
    pools:   1 pools, 1 pgs
    objects: 2 objects, 449 KiB
    usage:   1.1 GiB used, 139 GiB / 140 GiB avail
    pgs:     1 active+clean
```

# Appendix
## Further reading
https://computingforgeeks.com/install-ceph-storage-cluster-on-ubuntu-linux-servers/
https://docs.ceph.com/en/latest/cephadm/install/#cephadm-deploying-new-cluster
https://howardism.org/Technical/OpenStack/testing-heat-templates.html